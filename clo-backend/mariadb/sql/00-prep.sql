CREATE USER 'drupal'@'%' IDENTIFIED VIA mysql_native_password USING '*AAF4B8DB14CE6C8825D9DFE72D5B51EA1FA177B4';
GRANT USAGE ON *.* TO 'drupal'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE USER 'citrix'@'%' IDENTIFIED VIA mysql_native_password USING '*E272CFFCB160DCB4A37FE0EB32144E88274349C0';
GRANT SELECT, SHOW DATABASES, REPLICATION CLIENT, TRIGGER, SHOW VIEW ON *.* TO 'citrix'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
CREATE DATABASE IF NOT EXISTS `drupal_sit` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
CREATE DATABASE IF NOT EXISTS `drupal_qa` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
CREATE DATABASE IF NOT EXISTS `drupal_uat` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
CREATE DATABASE IF NOT EXISTS `drupal_prod` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL PRIVILEGES ON `drupal`.* TO 'drupal'@'%';
GRANT ALL PRIVILEGES ON `drupal\_%`.* TO 'drupal'@'%';
