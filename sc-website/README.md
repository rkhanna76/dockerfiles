```
# ./start_redis_cluster.sh

        Please select which environment to create redis cluster for:
        [ sit | qa | uat | prod ] # uat

        Creating redis cluster for environment: uat using redis port 6381 and cluster port 16381

>>> Creating cluster
>>> Performing hash slots allocation on 3 nodes...
Using 3 masters:
10.166.169.74:6381
10.166.169.78:6381
10.166.169.79:6381
M: 9fd96af3d3eca9c16b2864abe158c85423d27a06 10.166.169.74:6381
   slots:0-5460 (5461 slots) master
M: 1694e5c92811edca8dca0e5c86af56a4a59bab37 10.166.169.78:6381
   slots:5461-10922 (5462 slots) master
M: 9f68ba9cea35868888110d3899f4c58b5d720bd1 10.166.169.79:6381
   slots:10923-16383 (5461 slots) master
Can I set the above configuration? (type 'yes' to accept): yes
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join..
>>> Performing Cluster Check (using node 10.166.169.74:6381)
M: 9fd96af3d3eca9c16b2864abe158c85423d27a06 10.166.169.74:6381
   slots:0-5460 (5461 slots) master
   0 additional replica(s)
M: 9f68ba9cea35868888110d3899f4c58b5d720bd1 10.166.169.79:6381
   slots:10923-16383 (5461 slots) master
   0 additional replica(s)
M: 1694e5c92811edca8dca0e5c86af56a4a59bab37 10.166.169.78:6381
   slots:5461-10922 (5462 slots) master
   0 additional replica(s)
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```
