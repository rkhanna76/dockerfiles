#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

exec /usr/sbin/httpd -DFOREGROUND
