#!/bin/bash
#
# jdreiner@kdigital.com
#
# Requires ruby, redis-trib.sh and the redis rubygem:
#
# yum -y install ruby
# gem install redis
#

export node1ip=10.166.169.74
export node2ip=10.166.169.78
export node3ip=10.166.169.79

env="$1"

case $env in
  sit) export env="sit" ;;
  qa) export env="qa" ;;
  uat) export env="uat" ;;
  prod) export env="prod" ;;
  *) printf "\n\tPlease select which environment to create redis cluster for:\n"
     printf "\t[ sit | qa | uat | prod ] # "
     read env ;;
esac

if [[ $env == sit ]] ; then
    export port=6383
elif [[ $env == qa ]] ; then
    export port=6382
elif [[ $env == uat ]] ; then
    export port=6381
elif [[ $env == prod ]] ; then
    export port=6380
else
    printf "\n\tInvalid input. Please specify [ sit | qa | uat | prod ]\n" && exit 1
fi

printf "\n\tCreating redis cluster for environment: ${env} using redis port ${port} and cluster port 1${port}\n\n"

ruby redis-trib.rb create --replicas 0 ${node1ip}:${port} ${node2ip}:${port} ${node3ip}:${port}
