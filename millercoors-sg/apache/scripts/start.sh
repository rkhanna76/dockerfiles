#!/bin/bash

set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "The host is ${HOST_MACHINE} and the environment is ${APP_ENV}. The build number is #${BUILD_NUMBER}." >> /var/www/html/web/whoami.txt

# In case that there is a forgotten apache process we kill it here.
rm -f /var/run/httpd/httpd.pid

exec /usr/sbin/httpd -DFOREGROUND

