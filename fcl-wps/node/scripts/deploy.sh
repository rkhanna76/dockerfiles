#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

cd /app
git pull
npm install && npm cache clean

cd /ang_app
git pull
npm install && npm cache clean
ng build --prod

echo "Deployment Complete!"

exit 0
