# **IBM WebSphere Portal 8.5 with Web Content Manager **

This section contains images to build and run Websphere Portal 8.5 with either the CF12 or CF09 cumulative fixset applied. You can select which patch level you would like to build in the compose file, which is controlled via a docker build ARG.

When building on a local desktop, you must be connected to the Managed Services environment VPN in order to connect to config.k2digital.com. Alternately, you can save the packages on a local web server instead, defined with the build args.

