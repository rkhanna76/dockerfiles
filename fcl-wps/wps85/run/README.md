# **IBM WebSphere Portal 8.5 CF12 with Web Content Manager **

This dockerfile is for building a finalized WPS instance using the pre-created distribution stored on config.k2digital.com.

the wp_cf BUILD_ARG is used to determine the CF level (currently 09 and 12) to use when running the container.

This is for future customization. The finalized container will also be checked into the Docker Registry, which should be faster to launch for any projects where you just need a quick WPS environment


