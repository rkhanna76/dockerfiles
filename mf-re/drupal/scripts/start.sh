#!/bin/bash

set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "The host is ${HOST_MACHINE} and the environment is ${APP_ENV}. The build number is #${BUILD_NUMBER}." >> /var/www/html/whoami.txt

dbhost=dbhost
dbport=3306

echo -n "waiting for TCP connection to $dbhost:$dbport..."
while ! $(tcping -q -t 1 $dbhost $dbport); do
  sleep 8
  echo "still waiting for MariaDB to become available..."
done
echo "MariaDB started, ready to continue..."

echo "Prelaunch preparation complete, starting apache...."

# In case that there is a forgotten apache process we kill it here.
rm -f /var/run/httpd/httpd.pid
exec /usr/sbin/httpd -DFOREGROUND
