#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

export bld=$(tput bold)
export nrm=$(tput sgr0)

if [[ ${APP_ENV} == "SIT" ]] ; then
  export branch=develop
elif [[ ${APP_ENV} == "QA" ]] ; then
  export branch=release
else
  export branch=develop
  printf "\n\t${bld}WARNING:${nrm}\n\tAPP_ENV not SIT or QA. Defaulting to ${bld}${branch}${nrm} branch...\n\n"
fi

cd /var/www/html
printf "\n\t${APP_ENV} environment specified.\n\tChecking out origin/${bld}${branch}${nrm}...\n\n"
git reset --hard
git pull
git checkout ${branch}
git branch --set-upstream-to=origin/${branch} ${branch}
git pull -v origin ${branch}

printf "Build Host: ${HOST_MACHINE}\nBuild Container: $(/usr/bin/hostname)\nBuild Environment: ${APP_ENV}\nJenkins Build: build-${build_number}\nCommit Hash: ${git_commit}\nJob Details: ${build_url}\n" > /var/www/html/build.txt || true
set -u

printf "\n\tApply Best-Practice Permissions...\n\n"
chown -R apache: /var/www/html 

# drush cc
su apache << EOF
cd /var/www/html
set +u
printf "\n\tRun Drush cache clear...\n\n"
cd /var/www/html/sites/default
/usr/local/bin/drush cc all
EOF

printf "\n\t${bld}Deployment complete!${nrm}\n\n"

exit 0
