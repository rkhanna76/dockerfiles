#!/bin/bash

[[ -z "$1" ]] && { echo "Valid options: [qa] [uat] [prod]" ; exit 1; }

set -euo pipefail

env=$1

cd ../compose
docker-compose -p mfre up -d web-${env}
