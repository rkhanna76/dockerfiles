#!/bin/bash

[[ -z "$1" ]] && { echo "Valid options: [db-bootstrap] [db-host]" ; exit 1; }

set -euo pipefail

host=$1

cd ../compose
docker-compose -p mfre up -d ${host}
docker-compose logs
