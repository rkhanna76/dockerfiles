# Docker for MF Real Estate CMS

## Compose Files:
~~~
compose/
compose/docker-compose.yml - main project file
compose/web.yml - extends file for compose, do not use directly
~~~

## Building:

### Node 1:
~~~
cd compose
docker-compose -p mfre up -d db-boot # Bootstrap first node in mariadb galera cluster
docker-compose -p mfre up -d web-qa # QA build and deploy
docker-compose -p mfre up -d web-uat # UAT build and deploy
docker-compose -p mfre up -d web-prod # Production build and deploy
docker-compose -p mfre up -d pma # Install PhPMyAdmin
~~~

### Nodes 2 through N:
~~~
cd compose
docker-compose -p mfre up -d db
docker-compose -p mfre up -d web-qa
docker-compose -p mfre up -d web-uat
docker-compose -p mfre up -d web-prod
~~~
