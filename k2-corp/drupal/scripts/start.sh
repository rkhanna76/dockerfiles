#!/bin/bash

set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "The host is ${HOST_MACHINE} and the environment is ${APP_ENV}. The build number is #${BUILD_NUMBER}." >> /var/www/html/whoami.txt

# In case that there is a forgotten apache PID or SHM file(s) we kill them here.
rm -f /var/run/httpd/*
rm -f /run/httpd/*

dbhost=dbhost
dbport=3306

echo -n "MariaDB: Waiting for TCP connection to $dbhost:$dbport..."
while ! $(tcping -q -t 1 $dbhost $dbport); do
  sleep 8
  echo "still waiting for MariaDB to become available..."
done
echo "MariaDB started, ready to continue..."

exec /usr/sbin/httpd -DFOREGROUND

