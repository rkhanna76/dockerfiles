# Source this file before running docker-compose in order to define
# the environment variables required for the build environment
#
# source ./setenv.sh [ dev | qa | uat | prod ]

export git_url=git@github.com:k2digital/mf_loft.git

export assets_volume=/opt/assets:/assets
export mariadb_volume=/opt/mysql:/var/lib/mysql
export prod_web_exposed_port=81
export qa_web_exposed_port=82
export dev_web_exposed_port=83

export COMPOSE_PROJECT_NAME=mf_loft

export build_number=${BUILD_NUMBER:-nobuild}
export build_url=${BUILD_URL:-nobuild}
export build_tag=${BUILD_TAG:-$(date -u +"%Y%m%dT%H%M%SZ")}
export git_commit=${GIT_COMMIT:-$(git rev-parse HEAD)}

if [[ $1 = prod ]] ; then
  export build_env=prod
elif [[ $1 = qa ]] ; then
  export build_env=qa
elif [[ $1 = dev ]] ; then
  export build_env=develop
  export build_number=$(date -u +"%Y%m%dT%H%M")
  export assets_volume=/assets
  export mariadb_volume=/var/lib/mysql
else
  echo "Unable to determine environment, please specifiy qa/prod/dev"
fi

