# Docker for Sentry-Advisor YII2

Dockerfile uses build arg "BUILD_ENV" for building correct settings file for apache. BUILD_ENV can be one of PROD, UAT or QA:
```
docker build --rm --build-arg=BUILD_ENV=QA -t k2digital/sentry-advisor-qa
```
```
docker build --rm --build-arg=BUILD_ENV=UAT -t k2digital/sentry-advisor-uat
```

Run:
```
docker run -d -p 82:80 -p 2222:22 --name sentry-advisor-qa -t k2digital/sentry-advisor-qa
```
```
docker run -d -p 2221:22 --name sentry-advisor-uat -t k2digital/sentry-advisor-uat
```
