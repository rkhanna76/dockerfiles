#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

cd /var/www/html
git fetch
printf "\n\tChecking out origin/${build_env} into temporary local branch docker_deploy_build-${build_number}\n\n"
git checkout -b docker_deploy_build-${build_number} origin/${build_env}
git pull -v
drush -r /var/www/html cc all -v
printf "Container ID: $(/usr/bin/hostname)\nEnvironment: ${build_env}\nBuild: build-${build_number}\nCommit Hash: ${git_commit}\nJob Details: ${build_url}\n" > /var/www/html/Web/build.txt
