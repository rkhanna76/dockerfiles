#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "Apply best-practice permissions"
cd /var/www/html
find . -exec chown root:apache {} \;
find . -type f \-exec chmod 640 {} \;
find . -type d \-exec chmod 750 {} \;

echo "The host is ${HOST_MACHINE} and the environment is ${APP_ENV}" >> /var/www/html/whoami.txt

exec /usr/sbin/httpd -DFOREGROUND
