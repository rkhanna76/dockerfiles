#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "Apply best-practice permissions"
cd /var/www/html
find . -exec chown root:apache {} \;
find . -type f \-exec chmod 640 {} \;
find . -type d \-exec chmod 750 {} \;
cd /assets
chown -R apache: ${build_env}
find ${build_env} -type f \-exec chmod 644 {} \;
find ${build_env} -type d \-exec chmod 755 {} \;

# Create symlimks to shared files volume
sites="default"
for site in $sites ; do
  source=/assets/${build_env}/sites/${site}/files
  dest=/var/www/html/sites/${site}/files
  if [[ ! -L $dest ]] ; then
    echo "Symlinking $source in $dest..."
    ln -s $source $dest
  else
    echo "Symlink $dest already present, skipping..."
  fi
done

dbhost=mariadb
dbport=3306
echo -n "waiting for TCP connection to $dbhost:$dbport..."
while ! $(tcping -q -t 1 $dbhost $dbport); do
  sleep 8
  echo "still waiting for MariaDB to become available..."
done
echo "MariaDB started, ready to continue..."

echo "Clearing Drupal caches before startup..."
cd /var/www/html
drush -r /var/www/html cc all -v

echo "Prelaunch preparation complete, starting apache...."

exec /usr/sbin/httpd -DFOREGROUND
