#!/bin/bash

# This script should be run as root, normally via jenkins
# docker exec -ti -u root node /deploy.sh

set -euo pipefail
#set -x

export APP_ENV=${APP_ENV:-unknown}
export HOST_MACHINE=${HOST_MACHINE:-unknown}
export build_number=${build_number:-unknown}
export git_commit=${git_commit:-unknown}
export build_url=${build_url:-unknown}

if [[ ${APP_ENV} == "SIT" ]] ; then
  export branch=develop
elif [[ ${APP_ENV} == "QA" ]] ; then
  export branch=release
else
  export branch=release
  printf "\n\tWARNING:\n\tAPP_ENV not SIT or QA. Defaulting to ${branch} branch...\n\n"
fi

cd /app
printf "\n\t${APP_ENV} environment specified.\n\tChecking out origin/${branch}...\n\n"
git reset --hard
git pull
git checkout ${branch}
git branch --set-upstream-to=origin/${branch} ${branch}
git pull -v origin ${branch}
chown -R node: /app

su node << EOF
npm install
set +u
printf "Build Host: ${HOST_MACHINE}\nBuild Container: $(/bin/hostname)\nBuild Environment: ${APP_ENV}\nJenkins Build: build-${build_number}\nCommit Hash: ${git_commit}\nJob Details: ${build_url}\n" > /app/build.txt || true
set -u
EOF

printf "\n\tDeployment complete!\n\n"

exit
