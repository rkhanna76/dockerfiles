#!/bin/bash

set -euo pipefail
#set -x

export APP_ENV=${APP_ENV:-unknown}
export HOST_MACHINE=${HOST_MACHINE:-unknown}
export build_number=${build_number:-unknown}
export git_commit=${git_commit:-unknown}
export build_url=${build_url:-unknown}

export bld=$(tput bold)
export nrm=$(tput sgr0)

if [[ ${APP_ENV} == "SIT" ]] ; then
  export branch=develop
elif [[ ${APP_ENV} == "QA" ]] ; then
  export branch=release
else
  export branch=release
  printf "\n\t${bld}WARNING:${nrm}\n\tAPP_ENV not SIT or QA. Defaulting to ${bld}${branch}${nrm} branch...\n\n"
fi

cd /var/www/html
printf "\n\t${APP_ENV} environment specified.\n\tChecking out origin/${bld}${branch}${nrm}...\n\n"
git reset --hard
git pull
git checkout ${branch}
git branch --set-upstream-to=origin/${branch} ${branch}
git pull -v origin ${branch}
npm install
ng build --prod

set +u
printf "Build Host: ${HOST_MACHINE}\nBuild Container: $(/usr/bin/hostname)\nBuild Environment: ${APP_ENV}\nJenkins Build: build-${build_number}\nCommit Hash: ${git_commit}\nJob Details: ${build_url}\n" > /var/www/html/dist/build.txt || true
set -u

printf "\n\tApply Best-Practice Permissions...\n\n"
chown -R apache: /var/www/html

printf "\n\t${bld}Deployment complete!${nrm}\n\n"

exit 0
