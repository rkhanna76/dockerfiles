#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

export bld=$(tput bold)
export nrm=$(tput sgr0)

if [[ ${APP_ENV} == "SIT" ]] ; then
  export branch=develop
elif [[ ${APP_ENV} == "QA" ]] ; then
  export branch=release
else
  export branch=develop
  printf "\n\t${bld}WARNING:${nrm}\n\tAPP_ENV not SIT or QA. Defaulting to ${bld}${branch}${nrm} branch...\n\n"
fi

cd /var/www/html
printf "\n\t${APP_ENV} environment specified.\n\tChecking out origin/${bld}${branch}${nrm}...\n\n"
git reset --hard
git checkout ${branch}
git pull -v origin ${branch}

printf "Build Host: ${HOST_MACHINE}\nBuild Container: $(/usr/bin/hostname)\nBuild Environment: ${APP_ENV}\nJenkins Build: build-${build_number}\nCommit Hash: ${git_commit}\nJob Details: ${build_url}\n" > /var/www/html/mage.ce.src/build.txt

printf "\n\tApply Best-Practice Permissions...\n\n"
chown -R apache: /var/www/html 
cd /var/www/html/mage.ce.src
find . -type f -not -path "./media/*" -exec chmod 400 {} \;
find . -type d -not -path "./media/*" -exec chmod 500 {} \;
find var/ -type f -exec chmod 600 {} \; 
find var/ -type d -exec chmod 700 {} \; 
find media/ -type f -exec chmod 600 {} \;
find media/ -type d -exec chmod 700 {} \;
chmod 700 includes
chmod 600 includes/config.php
chmod 500 /var/www/html/piloti/scripts/*sh 
chmod 500 /var/www/html/mage.ce.src/shell/*
chmod 640 /var/www/html/mage.ce.src/app/etc/modules/*.xml

/clear_caches.sh

printf "\n\t${bld}Deployment complete!${nrm}\n\n"

exit 0
