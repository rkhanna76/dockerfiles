#!/bin/bash

set -euo pipefail
#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

printf "\n\tFlushing redis databases ${REDISCACHEDB} and ${REDISSESSIONDB}...\n\n"
redis-cli -h redis -n ${REDISCACHEDB} flushdb
redis-cli -h redis -n ${REDISSESSIONDB} flushdb

su apache << EOF
cd /var/www/html/mage.ce.src/shell
printf "\n\tDisable magento compilation...\n\n"
  php compiler.php clear
printf "\n\tClear Magento caches...\n\n"
  rm -f /var/www/html/mage.ce.src/media/js/*.js
  rm -f /var/www/html/mage.ce.src/media/css/*.css
  rm -f /var/www/html/mage.ce.src/media/css_secure/*.css
  ./n98-magerun.phar cache:clean
  ./n98-magerun.phar cache:flush
printf "\n\tRebuild Indexes...\n\n"
  ./n98-magerun.phar index:reindex:all
#printf "\n\tRe-enable magento compilation...\n\n"
#  php compiler.php compile
EOF

printf "\n\tCache and Index rebuilds complete!\n\n"
