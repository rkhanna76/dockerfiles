#!/bin/bash

#set -x

exec 1> >(logger -s -t $(basename $0)) 2>&1

echo "The host is ${HOST_MACHINE} and the environment is ${APP_ENV}. This was a build #${BUILD_NUMBER}." >> /var/www/html/mage.ce.src/whoami.txt

# In case that there is a forgotten apache process we kill it here.
rm -f /var/run/httpd/httpd.pid

reports_dir=/var/www/html/mage.ce.src/var/csv
if [[ -L ${reports_dir} && -d ${reports_dir} ]] ; then
  echo "${reports_dir} is a symlink to a directory. Leaving it intact."
else
  echo "${reports_dir} is not a symlink. Removing and recreating."
  rm -rf ${reports_dir}
  ln -s /var/www/html/mage.ce.src/media/reports ${reports_dir}
fi

dbhost=dbhost.k2
dbport=3306

echo -n "MariaDB: Waiting for TCP connection to $dbhost:$dbport..."
while ! $(tcping -q -t 1 $dbhost $dbport); do
  sleep 8
  echo "still waiting for MariaDB to become available..."
done
echo "MariaDB started, ready to continue..."

redishost=redis
redisport=6379

echo -n "Redis: Waiting for TCP connection to $redishost:$redisport..."
while ! $(tcping -q -t 1 $redishost $redisport); do
  sleep 8
  echo "still waiting for Redis to become available..."
done
echo "Redis started, ready to continue..."

/clear_caches.sh

echo "All pre-startup tasks completed, launching apache..."
exec /usr/sbin/httpd -DFOREGROUND
