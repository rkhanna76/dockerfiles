FROM dayreiner/centos7-php56:latest
MAINTAINER jdreiner@k2digital.com

# Setup htaccess and apache conf
COPY config/passwd /var/www/passwd
COPY config/php.ini /etc/php.ini
COPY config/docker_id_rsa /root/.ssh/id_rsa
COPY scripts/ /
COPY shared/ /var/www/shared/

RUN yum makecache fast && yum -y update && yum clean all && yum history new

ARG build_env
ARG git_url

# Deploy github codebase and build the site
RUN chmod 600 /root/.ssh/id_rsa \
    && mkdir -p /var/www/releases/1 && rm -rf /var/www/html \
    && ln -s /var/www/releases/1/html /var/www/current \
    && ln -s /var/www/current /var/www/html \
    && git clone --progress ${git_url} /var/www/releases/1 \
    && cd /var/www/releases/1 \
    && git checkout -b release \
    && git branch --set-upstream-to=origin/release release \
    && git pull

COPY config/site.conf.${build_env} /etc/httpd/conf.d/site.conf
COPY config/database.php.${build_env} /var/www/shared/application/config/live/database.php

RUN ln -s /var/www/shared/application/config/live/database.php /var/www/releases/1/application/config/database.php \
    && ln -s /var/www/shared/application/config/live/database.php /var/www/releases/1/application/config/live/database.php \
    && ln -s /var/www/shared/html/.htaccess /var/www/releases/1/html/.htaccess \
    && chown -R apache:apache /var/www

EXPOSE 80
ENTRYPOINT ["/start.sh"]
