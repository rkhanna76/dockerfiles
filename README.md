Ports used when building docker containers:
- prod: 80
- uat: 81
- qa: 82
- sit: 83

Local ports in use per project:
- Street Capital: 2001
- Manulife Real Estate: 2002

Xdebug is installed on the image used for local development. It is configured to work on port 9001.
When using Docker for Mac you need to run the folowing command upon restart:
sudo ifconfig lo0 alias 172.16.17.18
