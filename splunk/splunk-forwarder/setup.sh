#!/bin/bash

set -x

export hostname=$(hostname -s)

[[ ! -d /splunk/config ]] && mkdir -p /splunk/config
cp conf/ta-dockerstats-inputs.conf /splunk/config/ta-dockerstats-inputs.conf
cp conf/ta-dockerlogs-inputs.conf /splunk/config/ta-dockerlogs-inputs.conf

sed -i "s/%%HOSTNAME%%/${hostname}/g" /splunk/config/ta-dockerstats-inputs.conf
sed -i "s/%%HOSTNAME%%/${hostname}/g" /splunk/config/ta-dockerlogs-inputs.conf

